package controlador;
import modelo.Cotizacion;
import vista.dlgCotizacion;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
public class Controlador implements ActionListener{
    private Cotizacion C;
    private dlgCotizacion vista;
    public Controlador(Cotizacion C,dlgCotizacion vista){
        this.C = C;
        this.vista = vista;
        vista.btmCancelar.addActionListener(this);
        vista.btmCerrar.addActionListener(this);
        vista.btmGuardar.addActionListener(this);
        vista.btmLimpiar.addActionListener(this);
        vista.btmMostrar.addActionListener(this);
        vista.btmNuevo.addActionListener(this);
    }
    private void iniciarVista(){
        vista.setTitle(":: Productos ::");
        vista.setSize(560,650);
        vista.setLocationRelativeTo(null);
        vista.setVisible(true);
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==vista.btmLimpiar){
            vista.txtDescripcion.setText("");
            vista.txtNumCotizacion.setText("");
            vista.txtPagoInicial.setText("");
            vista.txtPagoMensual.setText("");
            vista.txtPorcentajePagoInicial.setText("");
            vista.txtPrecio.setText("");
            vista.txtTotalAFin.setText("");
            vista.cboPlazo.setEnabled(false);
        }
        
        if(e.getSource()==vista.btmNuevo){
            vista.txtNumCotizacion.setEnabled(true);
            vista.txtDescripcion.setEnabled(true);
            vista.txtPrecio.setEnabled(true);
            vista.txtPorcentajePagoInicial.setEnabled(true); 
            vista.btmMostrar.setEnabled(true); 
            vista.btmGuardar.setEnabled(true);
            vista.cboPlazo.setEnabled(true);
        }
        if(e.getSource()==vista.btmGuardar){
            C.setNumDeCotizacion(vista.txtNumCotizacion.getText());
            C.setDescripcion(vista.txtDescripcion.getText());
            try{
                float x=(Float.parseFloat(vista.txtPorcentajePagoInicial.getText()));
                if(x<=100){
                    C.setPorcentajeDePago(x);
                }
                else{
                    JOptionPane.showMessageDialog(vista,"El porcentaje no puede ser mayor a 100");
                    return;
                }
                C.setPrecio(Float.parseFloat(vista.txtPrecio.getText()));
                C.setPlazo(Integer.parseInt(vista.cboPlazo.getSelectedItem().toString()));
                JOptionPane.showMessageDialog(vista,"Se agrego Exitosamente"); 
            }catch (NumberFormatException ex){
                JOptionPane.showMessageDialog(vista, "Surgio el siguiente error : " + ex.getMessage());
            }
            catch(Exception ex2){
                JOptionPane.showMessageDialog(vista, "Surgio el siguiente error : " + ex2.getMessage());
            }
            vista.txtPagoMensual.setText(String.valueOf(C.cacularPagoMensual()));
            vista.txtTotalAFin.setText(String.valueOf(C.cacularPagoTotal()));
            vista.txtPagoInicial.setText(String.valueOf(C.cacularPagoIncial()));
        }
        
        if(e.getSource()==vista.btmMostrar){
            vista.txtDescripcion.setText(C.getDescripcion());
            vista.txtNumCotizacion.setText(C.getNumDeCotizacion());
            vista.txtPrecio.setText(String.valueOf(C.getPrecio()));
            vista.txtPorcentajePagoInicial.setText(String.valueOf(C.getporcentajeDePago()));
            vista.cboPlazo.setSelectedItem(String.valueOf(C.getPlazo()));
            vista.txtPagoMensual.setText(String.valueOf(C.cacularPagoMensual()));
            vista.txtTotalAFin.setText(String.valueOf(C.cacularPagoTotal()));
            vista.txtPagoInicial.setText(String.valueOf(C.cacularPagoIncial()));
        }
        
        if(e.getSource()==vista.btmCerrar){
            int option=JOptionPane.showConfirmDialog(vista,"Seguro que quieres salir",
                    "Decide",JOptionPane.YES_NO_OPTION);
            if(option==JOptionPane.YES_NO_OPTION){
                vista.dispose();
                System.exit(0);
            }
        }
        
        if(e.getSource()==vista.btmCancelar){
            vista.txtDescripcion.setText("");
            vista.txtNumCotizacion.setText("");
            vista.txtPagoInicial.setText("");
            vista.txtPagoMensual.setText("");
            vista.txtPorcentajePagoInicial.setText("");
            vista.txtPrecio.setText("");
            vista.txtTotalAFin.setText("");
            vista.txtNumCotizacion.setEnabled(false);
            vista.txtDescripcion.setEnabled(false);
            vista.txtPrecio.setEnabled(false);
            vista.txtPorcentajePagoInicial.setEnabled(false);
            vista.btmMostrar.setEnabled(false);
            vista.btmGuardar.setEnabled(false);
            vista.cboPlazo.setEnabled(false);
        }
    }
    
    public static void main(String[] args) {
        Cotizacion C=new Cotizacion();
        dlgCotizacion vista = new dlgCotizacion(new JFrame(),true);
                
        Controlador contra = new Controlador (C,vista);
        contra.iniciarVista();
    }
}